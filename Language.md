add a b c  ( a = b + c )
addc a b C (C is constant)
*** with others (add sub mul div)
setc a C  ( a = C )

:label
jmpc label

jmp a (PC = a)

jmpcz label a (PC = label if a = 0)
jmpz a b (PC = a if b = 0)
