//type SignalListener = (...a: any) => void;
type SignalListener<ArgumentTypes extends any[]> = (...a:ArgumentTypes)=>void;

export class Signal<ArgumentTypes extends any[]> {
    private listeners = new Set<(...a:ArgumentTypes)=>void>();

    public connect(listener: SignalListener<ArgumentTypes>) {
        this.listeners.add(listener);
    }

    public disconnect(listener: SignalListener<ArgumentTypes>) {
        this.listeners.delete(listener);
    }

    public disconnectAll() {
        this.listeners.clear();
    }

    public emitSignal(...args : ArgumentTypes) {
        let toBeDeleted = new Array<(...a:ArgumentTypes)=>void>();
        this.listeners.forEach(listener => {
            try {
                listener(...args)
            } catch (error) {
                toBeDeleted.push(listener);
            }
        });
        toBeDeleted.forEach(listener => {
            this.listeners.delete(listener);
        });
    }
}