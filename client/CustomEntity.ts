//#region PREAMBLE
/*
    This is an ASCII MMO game.
    Copyright (C) 2021 waleed177 <potatoxel@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, version 3 of the
    License only.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
//#endregion

import { context, keyboard } from "./Client.js";
import { Entity } from "./Entity.js";
import { Signal } from "./shared/Signal.js";
import { SpawnGameObjectData } from "./shared/SpawnGameObjectData.js";
import { Vector3 } from "./shared/Vector3.js";

export class CustomEntity extends Entity {
    metadata: Map<string, string> = new Map<string, string>();
    listeners: Map<string, Signal<[string, string]>> = new Map<string, Signal<[string, string]>>();
    image: CanvasImageSource;

    constructor() {
        super();
        this.size = new Vector3(32, 32, 0);
        this.bindListener("x", (k,v) => this.position.x = Number.parseFloat(v));
        this.bindListener("y", (k,v) => this.position.y = Number.parseFloat(v));
        this.bindListener("z", (k,v) => this.position.z = Number.parseFloat(v));
        this.bindListener("sx", (k,v) => this.size.x = Number.parseFloat(v));
        this.bindListener("sy", (k,v) => this.size.y = Number.parseFloat(v));
        this.bindListener("sz", (k,v) => this.size.z = Number.parseFloat(v));
        this.bindListener("image", (k,v) => {
            if (v != "") {
                let image = new Image();
                image.src = v; //TODO: DONT
                image.onload = () => {
                    this.image = image;
                }
            } else {
                this.image = null;
            }
        });
        this.messageHandler.on("set", (_, data) => {
            this.setMetadata(data.key, data.value, false);
        });
    }


    init(data: SpawnGameObjectData) {
        super.init(data);
        let data2 = data.data as [string, string][];
        data2.forEach((kv => {
            let key = kv[0];
            let value = kv[1];
            this.setMetadata(key, value, false);
        }));
    }

    setMetadata(key: string, value: string, emit: boolean = true){
        let old_eq_new = this.metadata.has(key) ? value == this.metadata.get(key) : false;
        if(this.listeners.has(key)) {
            this.listeners.get(key).emitSignal(key, value);
        }
        this.metadata.set(key, value);

        if(emit && !old_eq_new)
            this.emit("set", {key: key, value: value});
    }

    bindListener(key: string, func: (key: string, new_value: string)=> void) {
        if(!this.listeners.get(key)) {
            this.listeners.set(key, new Signal<[string, string]>());
        }
        this.listeners.get(key).connect(func);
    }

    update() {
        if(this.metadata.has("keyboard") && this.metadata.get("keyboard") == this.world.playerId + "") {
            this.setMetadata("keyboard:w", keyboard.isKeyDown("w") ? "true" : "false");
            this.setMetadata("keyboard:a", keyboard.isKeyDown("a") ? "true" : "false");
            this.setMetadata("keyboard:s", keyboard.isKeyDown("s") ? "true" : "false");
            this.setMetadata("keyboard:d", keyboard.isKeyDown("d") ? "true" : "false");
            this.setMetadata("keyboard:e", keyboard.isKeyDown("e") ? "true" : "false");
            this.setMetadata("keyboard:r", keyboard.isKeyDown("r") ? "true" : "false");
            this.setMetadata("keyboard:space", keyboard.isKeyDown("space") ? "true" : "false");
        }
    }

    draw() {
        if(this.metadata.has("color")) {
            context.fillStyle = this.metadata.get("color");
            context.fillRect(this.position.x, this.position.y, this.size.x, this.size.y);
        } else if(this.image) {
            context.drawImage(this.image, this.position.x, this.position.y, this.size.x, this.size.y);
        }
    }

}
