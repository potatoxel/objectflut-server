import { SpawnGameObjectData } from "../client/shared/SpawnGameObjectData";
import { NetworkEntity } from "./NetworkEntity";
import { Signal } from './../client/shared/Signal';
import { ClientHandler } from "./ClientHandler";

export class CustomEntity extends NetworkEntity {
    shouldBeSerialized: boolean = true;
    metadata: Map<string, string> = new Map<string, string>();
    listeners: Map<string, Signal<[string, string]>> = new Map<string, Signal<[string, string]>>();
    
    constructor() {
        super();
        this.prefab = "customEntity";
        this.messageHandler.on("set", (sender, data) =>{
            this.setMetadata(data.key, data.value, sender);
        });
    }

    setMetadata(key: string, value: string, emitExclude: ClientHandler = null){
        if(this.listeners.has(key)) {
            this.listeners.get(key).emitSignal(key, value);
        }
        this.metadata.set(key, value);
        this.emit("set", {key: key, value: value}, emitExclude ? [emitExclude] : []);
    }

    getPublicData(): SpawnGameObjectData {
        let metadata: [string, string][] = [];
        this.metadata.forEach((v,k) => {
            metadata.push([k,v]);
        })
        return {
            id: this.id,
            prefab: this.prefab,
            data: metadata
        }
    }

    bindListener(key: string, func: (key: string, new_value: string)=> void) {
        if(!this.listeners.get(key)) {
            this.listeners.set(key, new Signal<[string, string]>());
        }
        this.listeners.get(key).connect(func);
    }

    unbindListeners(key: string) {
        this.listeners.get(key).disconnectAll();
    }
}