//#region PREAMBLE
/*
    This is an ASCII MMO game.
    Copyright (C) 2021 waleed177 <potatoxel@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, version 3 of the
    License only.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
//#endregion

import { NetworkEntity } from './NetworkEntity';
import { SettingPositionData } from '../client/shared/SettingPositionData'
import { ClientHandler } from './ClientHandler';
import { Vector3 } from '../client/shared/Vector3';
import { GameObject } from '../client/shared/GameObject';

export class NetworkPlayer extends NetworkEntity {
    clientHandler: ClientHandler;
    shouldBeSerialized: boolean = false;
    
    constructor(clientHandler: ClientHandler) {
        super();
        this.prefab = "player";
        
    }

    public update() {
        
    }

    public ready() {
        
    }

    public load() {
        
    }
}
