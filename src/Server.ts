//#region PREAMBLE
/*
    This is an ASCII MMO game.
    Copyright (C) 2021 waleed177 <potatoxel@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, version 3 of the
    License only.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
//#endregion

import { ClientHandler } from './ClientHandler.js';
import { app } from './app';
import { NetworkWorld } from './NetworkWorld';
import net from 'net';
import { CustomEntity } from './CustomEntity.js';

export class Server {
    private clients = new Array<ClientHandler>();
    public world = new NetworkWorld(this);

    addClient(client: ClientHandler) {
        this.clients.push(client);
    }

    start() {
        app.ws('/server', (ws, req) => {
            var clientHandler = new ClientHandler(this, ws);
            clientHandler.initializeEvents();
            this.addClient(clientHandler);
            clientHandler.setup();
            ws.on("close", () => {
                this.clients.splice(this.clients.indexOf(clientHandler), 1);
            });
        });

        var server = net.createServer((client) => {
            let buffer = "";

            client.setEncoding('utf-8');

            client.on('data', (data) => {
                buffer += data.toString();
                while(buffer.indexOf("\n") >= 0) {
                    let index = buffer.indexOf("\n");
                    if(index >= 0) {
                        let str = buffer.substring(0, index);
                        buffer = buffer.substr(index+1);
                        let sp = str.split(" ");
                        let command = sp[0];
                        
                        try {
                            if (command == "entity_create") {
                                let entity = new CustomEntity();
                                this.world.addChild(entity);
                                client.write("entity_create " + entity.id + "\n");
                            } else if(command == "entity_remove") {
                                let id = Number.parseFloat(sp[1]);
                                let entity = this.world.getChild(id);
                                if(entity instanceof CustomEntity){
                                    this.world.removeChild(entity);
                                }
                            } else if(command == "entity_set") {
                                let id = Number.parseFloat(sp[1]);
                                let entity = this.world.getChild(id);
                                if(entity instanceof CustomEntity){
                                    entity.setMetadata(sp[2], sp[3]);
                                }
                            } else if (command == "entity_get") {
                                let id = Number.parseFloat(sp[1]);
                                let entity = this.world.getChild(id);
                                if(entity instanceof CustomEntity){
                                    let key = sp[2];
                                    if(entity.metadata.has(key)){
                                        let value = entity.metadata.get(key);
                                        client.write("entity_get " + entity.id + " " + key + " " + value + "\n");
                                    } else {
                                        client.write("fail " + str + "\n");
                                    }
                                    
                                }
                            } else if(command == "entity_watch") {
                                let id = Number.parseFloat(sp[1]);
                                let entity = this.world.getChild(id);
                                if(entity instanceof CustomEntity){
                                    entity.bindListener(sp[2], (key, value) => {
                                        client.write("entity_watch " + entity.id + " " + key + " " + value + "\n");
                                    });
                                }
                            } else if(command == "entity_unwatch") {
                                let id = Number.parseFloat(sp[1]);
                                let entity = this.world.getChild(id);
                                if(entity instanceof CustomEntity){
                                    entity.unbindListeners(sp[2]);
                                }
                            } else if(command == "entity_list") {
                                let str = "";
    
                                this.world.getChildren().forEach(go => {
                                    if(go instanceof CustomEntity){
                                        str += go.id + " ";
                                    }
                                });
    
                                client.write("entity_list " + str + "\n");
                            } else if(command == "player_list") {
                                let str = "";
                                
                                this.clients.forEach( client => {
                                    str += client.player.id + " ";
                                });
    
                                client.write("player_list " + str + "\n");
                            }
                        } catch (error) {
                            console.log("" + error);
                            try {
                                client.write("fail " + str + "\n");
                            } catch (error) {
                                console.log("failed to write fail message.");                            
                            }
                        }
                        
                    }
                }
            });

        });

        server.listen(2345);

        setInterval(() => {
            this.world.update();
        }, 100);
    }

    broadcast(type: string, data: any, except: ClientHandler[] = null) {
        var data_to_send = JSON.stringify({
            "type": type,
            "json": data
        });

        for(var i = 0; i < this.clients.length; i++) {
            if(except && except.indexOf(this.clients[i]) >= 0) continue;
            this.clients[i].emitString(data_to_send);
        }
    }
}
